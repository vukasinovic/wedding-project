<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $fillable = [
        'image_url',
        'blog_id',
        'portfolio_id'
    ];
}
