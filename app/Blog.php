<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title',
        'body'
    ];

    public function images()
    {
        return $this->hasMany('App\Images', 'blog_id', 'id');
    }
}
