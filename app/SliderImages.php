<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderImages extends Model
{
    protected $fillable = [
        'image_url'
    ];
}
