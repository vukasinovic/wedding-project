<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contacted extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $phone;
    public $text;
    public $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $phone, $text, $name)
    {
        $this->$email = $email;
        $this->$phone = $phone;
        $this->$text = $text;
        $this->$name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('michele@allrosefarm.com')
            ->view('emails.contacted');
    }
}
