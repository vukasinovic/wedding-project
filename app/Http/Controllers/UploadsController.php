<?php

namespace App\Http\Controllers;

use App\Images;
use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use \App\SliderImages;
use App\Http\Requests;
use \Exception;

class UploadsController extends Controller
{
    public function uploadSliderImage(Request $request) {
        sleep(1);
        try {
            if ($request->hasFile('file')) {
                $images = new SliderImages();
                $image = $request->file('file');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $request->file("file")->move(public_path('uploads'), $filename);

                $images->image_url = 'uploads/' . $filename;
                $images->save();
            }
        } catch(Exception $e) {
            Log::error($e);
        }
    }

    public function uploadImagesBlog(Request $request) {
        sleep(1);
        try {
            if($request->hasFile('file')) {
                $images = new Images();
                $image = $request->file('file');

                $filename = time() . '.' . $image->getClientOriginalExtension();

                $request->file("file")->move(public_path('uploads'), $filename);

                $images->blog_id = $request->id;
                $images->image_url = 'uploads/'.$filename;
                $images->save();
            }
        } catch(Exception $e) {
            Log::error($e);
        }
    }

    public function uploadImagesPortfolio(Request $request) {
        sleep(1);
        try {
            if ($request->hasFile('file')) {
                $images = new Images();
                $image = $request->file('file');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $request->file("file")->move(public_path('uploads'), $filename);

                $images->portfolio_id = $request->id;
                $images->image_url = 'uploads/' . $filename;
                $images->save();
            }
        } catch(Exception $e) {
            Log::error($e);
        }
    }

    public function deletePortfolioImage(Request $request)
    {
        $image = Images::find($request->id);
        $tokens = explode('/', $image->image_url);
        $imageName = $tokens[sizeof($tokens)-1];
        unlink(public_path('uploads/'.$imageName));
        $image->delete();
    }
}