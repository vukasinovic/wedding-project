<?php

namespace App\Http\Controllers;

use App\Images;
use Illuminate\Http\Request;
use App\Blog;
use JWTAuth;

use App\Http\Requests;

class BlogsController extends Controller
{

    public function getAllBlogs()
    {
        $blogs = Blog::with('images')->get();

        return response()->json($blogs);
    }

    public function getBlog(Request $request)
    {
        $blog = Blog::with('images')->find($request->id);
        return response()->json($blog);
    }

    public function postBlog (Request $request)
    {
        $blog = new Blog();
        $blog->title = $request->title;
        $blog->body = $request->body;
        $blog->save();

        if($blog->save()) {
            return json_encode($blog->id);
        }

    }

    public function updateBlog (Request $request)
    {
        $blog = Blog::find($request->id);
        $blog->title = $request->title;
        $blog->body = $request->body;
        $status = $blog->save();

        if($request->blog_id != null || $request->blog_id != '')
        {
            $image = Images::where('blog_id',$request->blog_id)->get();
            $tokens = explode('/', $image[0]->image_url);
            $imageName = $tokens[sizeof($tokens)-1];
            unlink(public_path('uploads/'.$imageName));
            $image[0]->delete();
        }

        if($status) {
            return "true";
        } else {
            return "false";
        }
    }

    public function deleteBlog(Request $request) {

        $blog = Blog::find($request->id);
        $status = $blog->delete();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }
}
