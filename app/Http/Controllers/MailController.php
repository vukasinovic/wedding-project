<?php

namespace App\Http\Controllers;

use App\Mail\Contacted;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        $data = [
            'email' => $request->email,
            'phone' => $request->phone,
            'text' => $request->message,
            'name' => $request->name
        ];

        Mail::send('emails.contacted', $data , function($message) {
            $message->to('michele@allrosefarm.com', 'Allrose Farm')
                ->subject('Request Date');
        });

        Mail::send([], [], function ($message) use ($data){
            $message->to($data['email'])
            ->subject('Allrose Farm')
            ->from('michele@allrosefarm.com')
            // here comes what you want
            ->setBody('We will be contacting you soon.');
        });
    }
}
