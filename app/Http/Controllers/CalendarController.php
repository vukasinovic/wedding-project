<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use App\Http\Requests;

class CalendarController extends Controller
{
   public function getAllBookedDates()
   {
       $dates = Calendar::where('booked', 1)->get();

       return json_encode($dates);
   }
    public function getDate(Request $request)
    {
        $date = Calendar::find($request->id);

        return json_encode($date);
    }

    public function getAllDates()
    {
        $dates = Calendar::all();

        return json_encode($dates);
    }

    public function postDate (Request $request)
    {
        $date = new Calendar();
        $date->start_date = $request->start_date;
        $date->end_date = $request->end_date;
        $date->booked = $request->booked;
        $date->description = $request->description;
        $status = $date->save();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }

    public function updateDate (Request $request)
    {
        $date = Calendar::find($request->id);
        $date->start_date = $request->start_date;
        $date->end_date = $request->end_date;
        $date->booked = $request->booked;
        $date->description = $request->description;
        $status = $date->save();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }

    public function deleteDate (Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $date = Calendar::find($request->id);
        $status = $date->delete();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }

}
