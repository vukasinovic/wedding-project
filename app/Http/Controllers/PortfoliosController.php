<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use JWTAuth;

use App\Http\Requests;

class PortfoliosController extends Controller
{
    public function getAllPortfolios()
    {
        $portfolio = Portfolio::with('images')->get();
        return response()->json($portfolio);
    }

    public function getPortfolio(Request $request)
    {
        $portfolio = Portfolio::with('images')->find($request->id);
        return response()->json($portfolio);
    }

    public function postPortfolio (Request $request)
    {
        $portfolio = new Portfolio();
        $portfolio->title = $request->title;
        $portfolio->description = $request->description;

        if($portfolio->save()) {
            return json_encode($portfolio->id);
        }
    }

    public function updatePortfolio (Request $request)
    {
        $portfolio = Portfolio::find($request->id);
        $portfolio->title = $request->title;
        $portfolio->description = $request->description;

        $status = $portfolio->save();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }

    public function deletePortfolio(Request $request) {
        $portfolio = Portfolio::find($request->id);
        $status = $portfolio->delete();

        if($status) {
            return "true";
        } else {
            return "true";
        }
    }
}
