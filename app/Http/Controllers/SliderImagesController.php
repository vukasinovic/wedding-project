<?php

namespace App\Http\Controllers;

use App\SliderImages;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class SliderImagesController extends Controller
{
    public function getAllSlides()
    {
        $slides = SliderImages::all();

        return json_encode($slides);

    }

    public function deleteSlide(Request $request) {
        $sliderImage = SliderImages::find($request->id);
        $tokens = explode('/', $sliderImage->image_url);
        $imageName = $tokens[sizeof($tokens)-1];
        if(File::exists(public_path().'/uploads/'.$imageName)) {
            File::delete(public_path().'/uploads/'.$imageName);
        }
        if(!File::exists(public_path().'/uploads/'.$imageName)) {
            $sliderImage->delete();
        }
    }
}
