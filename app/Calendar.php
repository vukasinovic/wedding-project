<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'start_date',
        'end_date',
        'booked',
        'description'
    ];

    protected $table = 'calendar';
}
