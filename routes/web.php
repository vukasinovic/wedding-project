<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Get all
Route::get('/api/blogs', 'BlogsController@getAllBlogs');
Route::get('/api/portfolios', 'PortfoliosController@getAllPortfolios');
Route::get('/api/calendar/all', 'CalendarController@getAllDates');
Route::get('/api/calendar', 'CalendarController@getAllBookedDates');
Route::get('/api/slider', 'SliderImagesController@getAllSlides');
// Get by ID
Route::get('/api/blogs/{id}', 'BlogsController@getBlog');
Route::get('/api/portfolios/{id}', 'PortfoliosController@getPortfolio');
Route::get('/api/calendar/{id}', 'CalendarController@getDate');
//Login
Route::resource('login', 'LoginController', ['only' => ['index']]);
Route::post('/login', 'LoginController@doLogin');

// ADD Middleware to routes
Route::group(['middleware' => 'jwt.auth'], function () {
//Add
    Route::post('/api/blogs/add', 'BlogsController@postBlog');
    Route::post('/api/portfolios/add', 'PortfoliosController@postPortfolio');
    Route::post('/api/calendar/add', 'CalendarController@postDate');
    Route::post('/api/blog/images/add/{id}', 'UploadsController@uploadImagesBlog');
    Route::post('/api/portfolio/images/add/{id}', 'UploadsController@uploadImagesPortfolio');
//Update
    Route::post('/api/blogs/update', 'BlogsController@updateBlog');
    Route::post('/api/portfolios/update', 'PortfoliosController@updatePortfolio');
    Route::post('/api/calendar/update', 'CalendarController@updateDate');
//Delete
    Route::post('/api/blogs/delete/{id}', 'BlogsController@deleteBlog');
    Route::post('/api/portfolios/delete/{id}', 'PortfoliosController@deletePortfolio');
    Route::post('/api/calendar/delete/{id}', 'CalendarController@deleteDate');
    Route::post('/api/slider/delete/{id}', 'SliderImagesController@deleteSlide');
    Route::post('/api/portfolio/image/delete/{id}', 'UploadsController@deletePortfolioImage');
//Upload
Route::post('/api/upload/slider', 'UploadsController@uploadSliderImage');
});

//Send Mail
Route::post('/api/mail', 'MailController@sendMail');
