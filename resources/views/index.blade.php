<!DOCTYPE html>
<html lang="en" ng-app="weddingProject" ng-controller="BladeController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no"/>

    <title ng-bind="'Allrose Farm - ' + $root.title">Allrose Farm - Home</title>
    <link rel="stylesheet" href="bower_components/fullcalendar/dist/fullcalendar.css"/>
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.min.css">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/swiper/swiper.min.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/camera.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/google-map.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/mailform.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/angular/angular.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-ui-mask/dist/mask.js"></script>

    <script type="text/javascript" src="bower_components/angular-ui-calendar/src/calendar.js"></script>
    <script type="text/javascript" src="bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script type="text/javascript" src="bower_components/fullcalendar/dist/gcal.js"></script>
    <script src="bower_components/angular-file-upload/dist/angular-file-upload.min.js"></script>

    <script src='js/device.min.js'></script>

    <script src="bower_components/lodash/lodash.js"></script>
    <script src="bower_components/angular-route/angular-route.min.js"></script>
    <script src="bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
    <script src="bower_components/restangular/dist/restangular.min.js"></script>

    <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="bower_components/angular-aria/angular-aria.min.js"></script>
    <script src="bower_components/angular-messages/angular-messages.min.js"></script>
    <script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="bower_components/angular-material/angular-material.min.js"></script>
    <script src="bower_components/tinymce/tinymce.js"></script>
    <script src="bower_components/angular-ui-tinymce/src/tinymce.js"></script>
    <script src="bower_components/angular-loading-overlay/dist/angular-loading-overlay.js"></script>
    <script src="bower_components/angular-loading-overlay-spinjs/dist/angular-loading-overlay-spinjs.js"></script>
    <script src="bower_components/ngmap/build/scripts/ng-map.min.js"></script>

    <script src="js/script.js"></script>
    <script src="js/app.js"></script>

    <script src="js/controllers.js"></script>
    <script src="js/services.js"></script>
    <script src="js/swiper/swiper.min.js"></script>

</head>

<body>
<div class="page">
    <!--========================================================
                              HEADER
    =========================================================-->
    <header ng-class="headerSection" ng-hide="hideHeader">
        <div id="stuck_container" class="stuck_container">
            <div class="container">
                <div class="brand" onclick="window.location='#/'">
                </div>
                <nav class="nav">
                    <ul class="sf-menu" data-type="navbar">
                      <li class="home" ng-class="{active: $route.current.activetab == 'home'}">
                            <a href="#/">Home</a>
                        </li>
                        <li class="calendar" ng-class="{active: $route.current.activetab == 'calendar'}">
                            <a href="#/calendar">Availability</a>
                        </li>
                        <li class="portfolio" ng-class="{active: $route.current.activetab == 'portfolio'}">
                            <a href="#/portfolio">Portfolio</a>
                        </li>
                        <li class="blog" ng-class="{active: $route.current.activetab == 'blog'}">
                            <a href="#/blog">Blog</a>
                        </li>
                        <li class="contact" ng-class="{active: $route.current.activetab == 'contact'}">
                            <a href="#/contact">Contact</a>
                        </li>
                        <li class="more">
                            <a href="#">More</a>
                            <ul style="right: 0;">
                                <li>
                                    <a href="#/faq">FAQ</a>
                                    <a href="#/vendors">Preffered Vendors</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div ng-view autoscroll="true"></div>
    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer class="footer-section" ng-hide="hideFooter">
        <section class="bg-primary">
            <div class="container">
                <div class="row">
                    <div class="grid_4" data-equal-group="7">
                        <address class="addr">
                            <span class="fa fa-map-marker"></span>
                            121 East Road Greenfield, NH, 03047
                        </address>
                    </div>
                    <div class="grid_4 bg-default tc" data-equal-group="7">
                        <address class="addr">
                            <span class="fa fa-mobile"></span>
                            <a href="callto:16035472441">+1 (603) 547-2441</a>
                        </address>
                    </div>
                    <div class="grid_4" data-equal-group="7">
                        <address class="addr">
                            <span class="fa fa-envelope"></span>
                            <a class="link1" href="mailto:michele@allrosefarm.com">michele@allrosefarm.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </section>
        <section class="footer-bottom">
               <div layout="row" layout-align="space-around center">
                   <div layout-align="start center">
                       <p class="copy">Copyright <span id="copyright-year"></span> |
                        <a class="hover_pf" href="#/privacy-policy">Privacy Policy</a> | <a class="hover_pf" href="#/faq">FAQ</a> | <a class="hover_pf" href="#/vendors">Preferred Vendors</a>
                       </p>
                   </div>

                   <ul class="social-list">
                       <li><a href="https://www.facebook.com/Allrosefarmcountryweddings/" target="_blank"><span class="fa fa-facebook"></span></a></li>
                       {{--<li><a href="#"><span class="fa fa-twitter"></span></a></li>--}}
                       {{--<li><a href="#"><span class="fa fa-pinterest"></span></a></li>--}}
                       <li><a href="https://www.instagram.com/allrosefarmcountryweddings/" target="_blank"><span class="fa fa-instagram"></span></a></li>
                       {{--<li><a href="#"><span class="fa fa-google-plus"></span></a></li>--}}
                   </ul>
                   </div>
               </div>
       </section>
    </footer>
</div>
</body>
</html>
