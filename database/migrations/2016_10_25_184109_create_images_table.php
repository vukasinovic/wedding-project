<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_url');
            $table->integer('blog_id')->nullable()->unsigned();
            $table->foreign('blog_id')
                ->references('id')
                ->on('blogs')
                ->nullable()
                ->onDelete('cascade');

            $table->integer('portfolio_id')->nullable()->unsigned();
            $table->foreign('portfolio_id')
                ->references('id')
                ->on('portfolios')
                ->nullable()
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
