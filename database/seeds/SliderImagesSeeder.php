<?php

use Illuminate\Database\Seeder;
use App\SliderImages;

class SliderImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slider_images')->delete();

        SliderImages::create(array(
            'image_url'  => 'images/page-1_slide01.jpg'
        ));

        SliderImages::create(array(
            'image_url'  => 'images/page-1_slide02.jpg'
        ));

        SliderImages::create(array(
            'image_url'  => 'images/page-1_slide03.jpg'
        ));
    }
}
