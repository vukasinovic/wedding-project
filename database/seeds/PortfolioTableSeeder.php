<?php

use Illuminate\Database\Seeder;
use App\Portfolio;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolios')->delete();

        Portfolio::create(array(
            'title'     => 'Adie & Dante Wedding October 8, 2016',
            'description'    => '<p>An autumn wedding at peak foliage season. The barn was transformed into a woodland retreat. For the reception, warm lighting helped to make the evening enchanted.<br/><strong>Photography:</strong> Amanda Murdock<br/><strong>Catering:</strong> Fiddleheads Farm<br/><strong>Tables:</strong> Monadnock Tents &</p>',
        ));

//        Portfolio::create(array(
//            'title'     => 'Portfolio 02',
//            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//        ));
//
//        Portfolio::create(array(
//            'title'     => 'Portfolio 03',
//            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//        ));
//
//        Portfolio::create(array(
//            'title'     => 'Portfolio 04',
//            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//        ));
//
//        Portfolio::create(array(
//            'title'     => 'Portfolio 05',
//            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//        ));
//
//        Portfolio::create(array(
//            'title'     => 'Portfolio 06',
//            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//        ));
    }
}
