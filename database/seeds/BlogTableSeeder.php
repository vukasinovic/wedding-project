<?php

use Illuminate\Database\Seeder;
use App\Blog;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->delete();
//        Blog::create(array(
//            'title'     => 'Blog 01',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
//
//        Blog::create(array(
//            'title'     => 'Blog 02',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
//
//        Blog::create(array(
//            'title'     => 'Blog 03',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
//
//        Blog::create(array(
//            'title'     => 'Blog 04',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
//
//        Blog::create(array(
//            'title'     => 'Blog 05',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
//
//        Blog::create(array(
//            'title'     => 'Blog 06',
//            'body'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
//
//        ));
    }
}
