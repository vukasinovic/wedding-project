<?php

use Illuminate\Database\Seeder;
use App\Images;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->delete();

        Images::create(array(
            'image_url'  => 'uploads/1477613735.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613738.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613739.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613741.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613743.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613744.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613746.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613748.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613749.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613751.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613752.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613754.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613755.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));

        Images::create(array(
            'image_url'  => 'uploads/1477613757.jpg',
            'blog_id'    => null,
            'portfolio_id'    => 1,
        ));
    }
}
