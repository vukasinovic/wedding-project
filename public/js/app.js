var weddingProject = angular.module('weddingProject', [
    'ngRoute',
    'weddingProjectControllers',
    'weddingProjectServices',
    'ngMaterial',
    'ui.tinymce',
    'ngSanitize',
    'angularFileUpload',
    'ui.calendar',
    'bsLoadingOverlay',
    'bsLoadingOverlaySpinJs',
    'ui.mask',
    'ngMap'
]).run(function(bsLoadingOverlayService, $rootScope, $route) {
    bsLoadingOverlayService.setGlobalConfig({
        templateUrl: 'bsLoadingOverlaySpinJs'
    });
    $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
        //Change page title, based on Route information
        $rootScope.title = $route.current.title;
    });
});

weddingProject.config(['$routeProvider', function($routeProvider) {


    $routeProvider.
    when('/admin', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController',
        title: 'Login'
    }).
    when('/admin/blog', {
        templateUrl: 'partials/admin/blog-admin.html',
        controller: 'BlogAdminController',
        title: 'Blogs Editor'
    }).
    when('/admin/portfolio', {
        templateUrl: 'partials/admin/portfolio-admin.html',
        controller: 'PortfolioAdminController',
        title: 'Portfolios Editor'
    }).
    when('/admin/slider', {
        templateUrl: 'partials/admin/slider-admin.html',
        controller: 'SliderAdminController',
        title: 'Slider Editor'
    }).
    when('/admin/calendar', {
        templateUrl: 'partials/admin/calendar-admin.html',
        controller: 'CalendarAdminController',
        title: 'Availability Editor'
    }).
    when('/', {
        templateUrl: 'partials/index.html',
        controller: 'MainController',
        title: 'Home',
        activetab: 'home'
    }).
    when('/portfolio', {
        templateUrl: 'partials/portfolio.html',
        controller: 'PortfolioController',
        title: 'Portfolio',
        activetab: 'portfolio'
    }).
    when('/portfolio/:portfolioId', {
        templateUrl: 'partials/portfolio-single-view.html',
        controller: 'PortfolioSingleController',
        title: 'Single Portfolio',
        activetab: 'portfolio'
    }).
    when('/calendar', {
        templateUrl: 'partials/calendar.html',
        controller: 'CalendarController',
        title: 'Availability',
        activetab: 'calendar'
    }).
    when('/blog', {
        templateUrl: 'partials/blog.html',
        controller: 'BlogController',
        title: 'Blogs',
        activetab: 'blog'
    }).
    when('/blog/:blogId', {
        templateUrl: 'partials/blog-single-view.html',
        controller: 'BlogSingleController',
        title: 'Single Blog',
        activetab: 'blog'
    }).
    when('/contact', {
        templateUrl: 'partials/contact.html',
        controller: 'ContactController',
        title: 'Contact Us',
        activetab: 'contact'
    }).
    when('/privacy-policy', {
        templateUrl: 'partials/privacy-policy.html',
        controller: 'PrivacyPolicyController',
        title: 'Privacy Policy',
        activetab: 'privacy-policy'
    }).
    when('/faq', {
        templateUrl: 'partials/faq.html',
        controller: 'FaqController',
        title: 'FAQ',
        activetab: 'Faq'
    }).
    when('/vendors', {
        templateUrl: 'partials/vendors.html',
        controller: 'VendorsController',
        title: 'Preferred Vendors',
        activetab: 'vendors'
    }).
    otherwise({
        redirectTo: '/'
    });

}]);

weddingProject.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
weddingProject.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + ' ' + number.slice(3,7);
            }
            else{
                number = number;
            }

                return (" (" + city + ") " + number).trim();
            }
            else{

                return "(" + city;
            }

    };
});