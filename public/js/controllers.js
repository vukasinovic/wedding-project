var weddingProjectControllers = angular.module('weddingProjectControllers', []);


weddingProjectControllers.controller('LoginController', ['$scope', '$location', 'userService', '$mdToast', function ($scope, $location, userService, $mdToast) {

    $scope.email = '';
    $scope.password = '';

    if(userService.checkIfLoggedIn()) {
        $location.path('/admin/blog');
    }

    $scope.login = function() {
        if($scope.email == '' && $scope.password == '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your email and password!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        } else if($scope.email == '' && $scope.password != '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your email!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        } else if($scope.email != '' && $scope.password == '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your password!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        }
        else {
            userService.login(
                $scope.email, $scope.password,
                function(response){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Successfully logged in!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                    $location.path('/admin/blog');
                },
                function(response){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Wrong username or password!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                }
            );
        }
    }

    $scope.pressEnter = function(keyEvent) {
        if (keyEvent.which === 13) {
            if($scope.email == '' && $scope.password == '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your email and password!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            } else if($scope.email == '' && $scope.password != '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your email!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            } else if($scope.email != '' && $scope.password == '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your password!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            }
            else {
                userService.login(
                    $scope.email, $scope.password,
                    function(response){
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully logged in!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        $location.path('/admin/blog');
                    },
                    function(response){
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Wrong username or password!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                );
            }
        }
    }

}]);

weddingProjectControllers.controller('MainController', ['$scope', '$location', 'HttpGetServices', '$interval', 'NgMap', function ($scope, $location, HttpGetServices, $interval, NgMap) {

    NgMap.getMap().then(function(map) {

    });

    HttpGetServices.getListData(HttpGetServices.endpointSlider)
        .then(function(data) {
            $scope.slider = data;
            $scope.activated = false;
            setTimeout(function() {
                var swiper1 = new Swiper('.s1', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    loop: true,
                    centeredSlides: true,
                    autoplay: 3000,
                    autoplayDisableOnInteraction: false
                });
            }, 500);
    });

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);

}]);

weddingProjectControllers.controller('CalendarController', ['$scope','$rootScope', '$location', '$filter', '$http', '$q','uiCalendarConfig', 'HttpGetServices', function ($scope, $rootScope, $location, $filter, $http, $q, uiCalendarConfig, HttpGetServices) {
    $scope.selectedDate = null;
    $scope.firstDayOfWeek = 0;


    //CONFIG CALENDAR
    $scope.events = [];

    $scope.eventSources = [$scope.events];
    //END - CONFIG CALENDAR
    HttpGetServices.getListData(HttpGetServices.endpointCalendarList).then(function (response) {
        $scope.events.splice(0, $scope.events.length);
        $scope.bookedDates = response;
        angular.forEach($scope.bookedDates, function (value, key) {
            $scope.events.push({
                title: value.description,
                start: new Date(value.start_date),
                end: new Date(value.end_date),
                className: 'calEvent',
                allDay: true,
                stick: true
            });
        });
    });

    $scope.eventClick = function(date, jsEvent, view) {

    };

    $scope.dayClick = function(date, jsEvent, view) {
        function convert(str) {
            var date = new Date(str),
                mnth = ("0" + (date.getMonth()+1)).slice(-2),
                day  = ("0" + date.getDate()).slice(-2);
            return [ date.getFullYear(), mnth, day ].join("-");
        }
        $scope.msg = "\r\n\r\n\r\n\r\n\r\nRequested date: " + convert(date);
        $rootScope.contactMessage = $scope.msg;
        $location.path("/contact");
    };

    $scope.uiConfig = {
        calendar:{
            editable: false,
            header:{
                left: '',
                center: 'title',
                right: 'today prev,next'
            },
            eventClick: $scope.eventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            dayClick: $scope.dayClick
        }
    };


}]);

weddingProjectControllers.controller('PortfolioController', ['$scope', '$location', 'HttpGetServices', 'parseHtmlService', '$interval', function ($scope, $location, HttpGetServices, parseHtmlService, $interval) {

    $scope.activated = true;

    HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
        .then(function(data) {
            $scope.portfolios = data;
            $scope.activated = false;
    });

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };

}]);

weddingProjectControllers.controller('PortfolioSingleController', ['$scope', '$location', '$routeParams', 'HttpGetServices', 'parseHtmlService', '$interval', function ($scope, $location, $routeParams, HttpGetServices, parseHtmlService, $interval) {

    var id = $routeParams.portfolioId;
    $scope.activated = true;

    HttpGetServices.getSingleData(HttpGetServices.endpointSinglePortfolio, id)
        .then(function(data) {
            $scope.portfolio = data;
            $scope.portfolioImages = data.images;
            $scope.activated = false;
            $scope.parseHtml = function() {
                return parseHtmlService.parse(data.description);
            };
            setTimeout(function() {
                var swiper2 = new Swiper('.s2', {
                    pagination: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    slidesPerView: 3,
                    spaceBetween: 30
                });
            }, 100);
    });

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);

}]);

weddingProjectControllers.controller('BlogController', ['$scope', '$location', 'HttpGetServices', 'parseHtmlService', '$interval', function ($scope, $location, HttpGetServices, parseHtmlService, $interval) {

    $scope.activated = true;
    $scope.blogs = null;

    HttpGetServices.getListData(HttpGetServices.endpointBlogList)
        .then(function(data) {
            $scope.blogs = data;
            $scope.activated = false;
    });

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };

}]);

weddingProjectControllers.controller('BlogSingleController', ['$scope', '$location', '$routeParams', 'HttpGetServices', 'parseHtmlService', '$interval', function ($scope, $location, $routeParams, HttpGetServices, parseHtmlService, $interval) {

    $scope.activated = true;

    var id = $routeParams.blogId;

    HttpGetServices.getSingleData(HttpGetServices.endpointSingleBlog, id)
        .then(function(data) {
            $scope.blog = data;
            $scope.activated = false;
            $scope.parseHtml = function() {
                return parseHtmlService.parse(data.body);
            };
    });

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);


}]);


weddingProjectControllers.controller('ContactController', ['$scope','$route', '$mdToast', 'HttpPostServices', function ($scope, $route, $mdToast, HttpPostServices) {
    $('.calendar').removeClass('active sfHover');
    
    $scope.sendMail = function() {
        var name = $scope.name;
        var email = $scope.email;
        var phone = $scope.phonenumber;
        var message = $scope.contactMessage;

        if((name != undefined && name != "" && name != null) && (email != undefined  && email != "" && email != null) && (phone != undefined && phone != "" && phone != null) && (message != undefined && message != "" && message != null)) {
            
            phone = "+1" + $scope.phonenumber;
            var params = {
                name: name,
                email: email,
                phone: phone,
                message: message
            };
            console.log(params);
            var token = "";
            HttpPostServices.postData(HttpPostServices.endpointSendMail, params, token,
                function(response) {
                    console.log(response.data);
                },
                function(response) {
                    console.log(response.data)
                }
            );

            console.log("Name: " + name + " Email: " + email + " Phone: " + phone + " Message: " + message);

        } else {
            console.log("Fill out all fields");
        }

    }

}]);

weddingProjectControllers.controller('PrivacyPolicyController', ['$scope', function ($scope) {

}]);
weddingProjectControllers.controller('FaqController', ['$scope', function ($scope) {

}]);

weddingProjectControllers.controller('VendorsController', ['$scope', function ($scope) {

}]);

weddingProjectControllers.controller('BladeController', ['$scope', '$location', '$rootScope', '$route', 'HideElementsService', function ($scope, $location, $rootScope, $route, HideElementsService) {

    HideElementsService.hideElements();

      $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
        $scope.$route = $route;

        HideElementsService.hideElements();

      });

}]);

// ADMIN PANEL
weddingProjectControllers.controller('AdminMenuController', ['$scope', '$location', 'userService','$mdSidenav', function ($scope, $location, userService, $mdSidenav) {
    $scope.logOut = function(){
        userService.logout();
        $location.path('/admin');
    };

    $scope.goto = function(root) {
        $location.path(root);
    };
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      }
    }
}]);

weddingProjectControllers.controller('BlogAdminController', ['$scope', '$location', 'HttpGetServices', '$mdDialog', '$rootScope', 'userService', 'HttpPostServices', 'parseHtmlService', '$mdToast', 'FileUploader', '$interval', function ($scope, $location, HttpGetServices, $mdDialog, $rootScope, userService, HttpPostServices, parseHtmlService, $mdToast, FileUploader, $interval) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.activated = true;

        $scope.determinateValue = 30;

        $interval(function() {

        $scope.determinateValue += 10;
        if ($scope.determinateValue > 100) {
          $scope.determinateValue = 10;
        }

        }, 100);

        $scope.currentNavItem = 'blog';
        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
            .then(function(data) {
                $scope.blogs = data;
                $scope.activated = false;
            });

        $scope.parseHtml = function(data) {
            return parseHtmlService.parse(data);
        };

        $scope.addBlog = function(ev) {

            if(userService.checkIfLoggedIn() == true) {
                $mdDialog.show({
                    controller: 'AddBlogPopUpController',
                    templateUrl: 'partials/admin/popups/add-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                    if(answer == 'add') {
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                            });
                    }
                    if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                }, function() {
                });

            } else {
                userService.logout();
                $location.path('/admin');
            }
        };

        $scope.editBlog = function(ev, id) {

            if(userService.checkIfLoggedIn() == true) {
                $rootScope.editBlogId = id;
                $mdDialog.show({
                    controller: 'EditBlogPopUpController',
                    templateUrl: 'partials/admin/popups/edit-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                    if(answer == 'edit') {
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                            });
                    }
                    if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                }, function() {
                });

            } else {
                userService.logout();
                $location.path('/admin');
            }

        };

        $scope.deleteBlog = function(ev, id) {

            if(userService.checkIfLoggedIn() == true) {
                $rootScope.deleteBlogId = id;
                $mdDialog.show({
                    controller: 'DeleteBlogPopUpController',
                    templateUrl: 'partials/admin/popups/delete-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                  if (answer == 'yes') {
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                                console.log(data);
                            });
                  }
                  if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                   }
                }, function() {
                });
            } else {
                userService.logout();
                $location.path('/admin');
            }

        };

    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);

weddingProjectControllers.controller('AddBlogPopUpController', ['$scope', 'HttpPostServices', 'userService', '$mdDialog', '$mdToast', '$location', 'FileUploader', 'bsLoadingOverlayService', function ($scope, HttpPostServices, userService, $mdDialog, $mdToast, $location, FileUploader, bsLoadingOverlayService) {
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/blog/images/add/',
        queueLimit: 1,
        headers: {'Authorization': 'Bearer ' + userService.getCurrentToken()}
    });

    $scope.isDisabled = false;
    $scope.answer = function(answer) {
        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'add') {

                var title = $scope.title;
                var body = $scope.tinymceModel;
                var itemsChosed = $scope.uploader.queue.length;


                if((title == null || title == "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (body != null || body != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if(itemsChosed == 0) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Chose Blog Image!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {

                    var params = {
                        title: title,
                        body: body
                    };
                    var token = userService.getCurrentToken();
                        HttpPostServices.postData(HttpPostServices.endpointAddNewBlog, params, token,
                            function (response) {

                                var idBlog = response.data;
                                var token = userService.getCurrentToken();

                                uploader.onBeforeUploadItem = function(item) {
                                    item.url = 'api/blog/images/add/' + idBlog;
                                };

                                $scope.isDisabled = true;
                                bsLoadingOverlayService.start();
                                var arr = uploader.getNotUploadedItems();
                                angular.forEach(arr, function (value, key) {
                                    uploader.uploadItem(value);
                                });

                                uploader.onCompleteItem = function (fileItem, response, status, headers) {
                                    //console.info('onCompleteItem', fileItem, response, status, headers);
                                    //console.log(response);
                                    //imageUrls.push();

                                };
                                uploader.onErrorItem = function (fileItem, response, status, headers) {
                                    bsLoadingOverlayService.stop();
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent("Something went wrong with uploading image!")
                                            .position('bottom left')
                                            .hideDelay(4000)
                                    );
                                    $mdDialog.hide(answer);
                                };

                                uploader.onSuccessItem = function(item, response, status, headers) {
                                    bsLoadingOverlayService.stop();

                                    $mdDialog.hide(answer);

                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Successfully added new blog!')
                                            .position('bottom left')
                                            .hideDelay(1000)
                                    );
                                }

                                uploader.onCompleteAll = function (fileItem, response, status, headers) {
                                    
                                };
                            },
                            function (response) {
                                if(response.data.error == "token_not_provided") {
                                    $mdDialog.hide('logout');
                                    userService.logout();
                                    $location.path('/admin');
                                }
                            }
                        );
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }

        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
        }
    };

}]);

weddingProjectControllers.controller('EditBlogPopUpController', ['$scope', '$rootScope', 'HttpGetServices', 'HttpPostServices', 'userService', '$mdDialog', '$mdToast', '$location', 'FileUploader', 'bsLoadingOverlayService',  function ($scope, $rootScope, HttpGetServices, HttpPostServices, userService, $mdDialog, $mdToast, $location, FileUploader, bsLoadingOverlayService) {
    
    var id = $rootScope.editBlogId;
    $scope.isDisabled = false;
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/blog/images/add/',
        queueLimit: 1,
        headers: {'Authorization': 'Bearer ' + userService.getCurrentToken()}
    });

    HttpGetServices.getSingleData(HttpGetServices.endpointSingleBlog, id)
        .then(function(data) {
            $scope.tinymceModel = data.body;
            $scope.title = data.title;
            for (var i = 0; i < data.images.length; i++) {
                var image = new FileUploader.FileItem($scope.uploader, {
                    lastModifiedDate: data.images[i].updated_at,
                    size: 1e6,
                    type: data.images[i].image_url,
                    name: data.images[i].id
                });

                image.progress = 1000;
                image.isUploaded = true;
                image.isSuccess = true;

                $scope.uploader.queue.push(image);
            }
    });

    $scope.answer = function(answer) {

        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'edit') {
                var id = $rootScope.editBlogId;
                var title = $scope.title;
                var body = $scope.tinymceModel;
                var itemsChosed = $scope.uploader.queue.length;


                if((title == null || title == "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (body != null || body != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if(itemsChosed == 0) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Chose Blog Image!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {

                    var params;

                    if(uploader.getNotUploadedItems().length != 0) {
                        params = {
                            id: id,
                            title: title,
                            body: body,
                            blog_id: id
                        };

                    } else {
                        params = {
                            id: id,
                            title: title,
                            body: body,
                            blog_id: ""
                        };
                    }

                    var token = userService.getCurrentToken();
                    HttpPostServices.postData(HttpPostServices.endpointEditBlog, params, token,
                        function(response) {

                            if(uploader.getNotUploadedItems().length != 0) {
                                var idBlog = $rootScope.editBlogId;

                                uploader.onBeforeUploadItem = function(item) {
                                    item.url = 'api/blog/images/add/' + idBlog;
                                };

                                $scope.isDisabled = true;
                                bsLoadingOverlayService.start();
                                var arr = uploader.getNotUploadedItems();
                                angular.forEach(arr, function (value, key) {
                                        uploader.uploadItem(value);
                                });

                                uploader.onCompleteItem = function (fileItem, response, status, headers) {
                                    //console.log(response);
                                    //console.info('onCompleteItem', fileItem, response, status, headers);
                                    //console.log(response);
                                    //imageUrls.push();

                                };
                                uploader.onErrorItem = function (fileItem, response, status, headers) {
                                    //console.info('onErrorItem', fileItem, response, status, headers);
                                    bsLoadingOverlayService.stop();
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent("Something went wrong with uploading image!")
                                            .position('bottom left')
                                            .hideDelay(4000)
                                    );
                                    $mdDialog.hide(answer);
                                };

                                uploader.onSuccessItem = function(item, response, status, headers) {
                                    bsLoadingOverlayService.stop();

                                    $mdDialog.hide(answer);

                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Successfully edited blog!')
                                            .position('bottom left')
                                            .hideDelay(1000)
                                    );
                                }

                                uploader.onCompleteAll = function (fileItem, response, status, headers) {
                                    //console.log(response);
                                    
                                };

                            } else {
                                $mdDialog.hide(answer);

                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Successfully edited blog!')
                                        .position('bottom left')
                                        .hideDelay(1000)
                                );
                            }

                        },
                        function(response) {
                            if(response.data.error == "token_not_provided") {
                                $mdDialog.hide('logout');
                                userService.logout();
                                $location.path('/admin');
                            }
                        }
                    );
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }

    };

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
        }
    };
}]);

weddingProjectControllers.controller('DeleteBlogPopUpController', ['$scope', '$rootScope', 'HttpPostServices', 'userService', '$mdDialog', '$location', '$mdToast', function ($scope, $rootScope, HttpPostServices, userService, $mdDialog, $location, $mdToast) {

    $scope.answer = function(answer) {
        if(userService.checkIfLoggedIn() == true) {
            if(answer == 'yes') {
                id = $rootScope.deleteBlogId;
                var token = userService.getCurrentToken();
                HttpPostServices.postData(HttpPostServices.endpointDeleteBlog + id, id, token,
                    function(response) {
                        $mdDialog.hide(answer);
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted blog!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    },
                    function(response) {
                        if(response.data.error == "token_not_provided") {
                            $mdDialog.hide('logout');
                            userService.logout();
                            $location.path('/admin');
                        }
                    }
                );
            } else if (answer == 'no') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };

}]);

weddingProjectControllers.controller('PortfolioAdminController', ['$scope', '$location', 'HttpGetServices', '$mdDialog', 'userService', 'parseHtmlService', '$rootScope', '$mdToast', '$interval', function ($scope, $location, HttpGetServices, $mdDialog, userService, parseHtmlService, $rootScope, $mdToast, $interval) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.activated = true;

        $scope.determinateValue = 30;

        $interval(function() {

        $scope.determinateValue += 10;
        if ($scope.determinateValue > 100) {
          $scope.determinateValue = 10;
        }

        }, 100);

        $scope.currentNavItem = 'portfolio';

        HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
            .then(function(data) {
                $scope.portfolios = data;
                $scope.activated = false;
            });

        $scope.parseHtml = function(data) {
            return parseHtmlService.parse(data);
        };

        $scope.addPortfolio = function(ev) {

            if(userService.checkIfLoggedIn() == true) {
                $mdDialog.show({
                    controller: 'AddPortfolioPopUpController',
                    templateUrl: 'partials/admin/popups/add-portfolio-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                  if(answer == 'add') {
                        HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
                            .then(function(data) {
                                $scope.portfolios = data;
                            });
                    }
                    if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                }, function(answer) {
                });

            } else {
                userService.logout();
                $location.path('/admin');
            }

        };

        $scope.editPortfolio = function(ev, id) {
            $rootScope.editPortfolioId = id;
              $mdDialog.show({
                controller: 'EditPortfolioPopUpController',
                templateUrl: 'partials/admin/popups/edit-portfolio-pop-up.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen
            })
            .then(function(answer) {
                if(answer == 'edit') {
                    HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
                        .then(function(data) {
                            $scope.portfolios = data;
                        });
                }
                if(answer == 'logout') {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('You have to login first!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                }
            }, function(answer) {
            });
        };

        $scope.deletePortfolio = function(ev, id) {

            if(userService.checkIfLoggedIn() == true) {
                $rootScope.deletePortfolioId = id;
                  $mdDialog.show({
                    controller: 'DeletePortfolioPopUpController',
                    templateUrl: 'partials/admin/popups/delete-portfolio-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                  if (answer == 'yes') {
                        HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
                            .then(function(data) {
                                $scope.portfolios = data;
                            });
                  }
                  if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                   }
                }, function() {
                });
            } else {
                userService.logout();
                $location.path('/admin');
            }
        };

    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);

weddingProjectControllers.controller('AddPortfolioPopUpController', ['$scope', 'HttpPostServices', '$mdDialog', 'userService', '$location', '$mdToast', 'FileUploader', 'bsLoadingOverlayService', function ($scope, HttpPostServices, $mdDialog, userService, $location, $mdToast, FileUploader, bsLoadingOverlayService) {

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/portfolio/images/add/',
        headers: {'Authorization': 'Bearer ' + userService.getCurrentToken()}
    });
    $scope.isDisabled = false;
    $scope.answer = function(answer) {
        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'add') {
                var title = $scope.title;
                var description = $scope.tinymceModel;
                var itemsChosed = $scope.uploader.queue.length;

                if((title == null || title == "") && (description == null || description == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (description != null || description != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (description == null || description == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if(itemsChosed == 0) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Chose Portfolio Images!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {
                    var params = {
                        title: title,
                        description: description
                    };

                    var token = userService.getCurrentToken();
                    HttpPostServices.postData(HttpPostServices.endpointAddNewPortfolio, params, token,
                        function(response) {

                            var idPortfolio = response.data;
                            var token = userService.getCurrentToken();

                            uploader.onBeforeUploadItem = function(item) {
                                item.url = 'api/portfolio/images/add/' + idPortfolio;
                            };

                            $scope.isDisabled = true;
                            bsLoadingOverlayService.start();                            
                            var arr = uploader.getNotUploadedItems();
                            angular.forEach(arr, function (value, key) {
                                    uploader.uploadItem(value);
                            });

                            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                                //console.info('onCompleteItem', fileItem, response, status, headers);
                                //console.log(response);
                                //imageUrls.push();

                            };
                            uploader.onErrorItem = function (fileItem, response, status, headers) {
                                //console.info('onErrorItem', fileItem, response, status, headers);
                                bsLoadingOverlayService.stop();
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent("Can't upload! Too large picture size!")
                                        .position('bottom left')
                                        .hideDelay(1000)
                                );
                            };
                            uploader.onCompleteAll = function (fileItem, response, status, headers) {
                                bsLoadingOverlayService.stop();

                                $mdDialog.hide(answer);

                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Successfully added new portfolio!')
                                        .position('bottom left')
                                        .hideDelay(1000)
                                );
                            };
                        },
                        function(response) {
                            if(response.data.error == "token_not_provided") {
                                $mdDialog.hide('logout');
                                userService.logout();
                                $location.path('/admin');
                            }
                        }
                    );
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }

        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };


    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
        }
    };

}]);

weddingProjectControllers.controller('EditPortfolioPopUpController', ['$scope', '$rootScope', '$mdDialog', 'HttpGetServices', 'HttpPostServices', 'userService', '$location', '$mdToast', 'FileUploader', 'bsLoadingOverlayService', function ($scope, $rootScope, $mdDialog, HttpGetServices, HttpPostServices, userService, $location, $mdToast, FileUploader, bsLoadingOverlayService) {
    var id = $rootScope.editPortfolioId;
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/blog/images/add/',
        headers: {'Authorization': 'Bearer ' + userService.getCurrentToken()}
    });
    
    $scope.isDisabled = false;
    $scope.removeFromDb = function(id) {
        var token = userService.getCurrentToken();
        HttpPostServices.postData(HttpPostServices.endpointDeletePortfolioImage + id, id, token,
            function(response) {
            },
            function(response) {
                console.log(response);
                if(response.data.error == "token_not_provided") {
                    $mdDialog.hide('logout');
                    userService.logout();
                    $location.path('/admin');
                }
            }
        );
    };

    HttpGetServices.getSingleData(HttpGetServices.endpointSinglePortfolio, id)
        .then(function(data) {
            $scope.tinymceModel = data.description;
            $scope.title = data.title;
            for (var i = 0; i < data.images.length; i++) {
                var image = new FileUploader.FileItem($scope.uploader, {
                    lastModifiedDate: data.images[i].updated_at,
                    size: 1e6,
                    type: data.images[i].image_url,
                    name: data.images[i].id,
                });

                image.progress = 1000;
                image.isUploaded = true;
                image.isSuccess = true;

                $scope.uploader.queue.push(image);
            }
    });

    $scope.answer = function(answer) {

        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'edit') {
                var id = $rootScope.editPortfolioId;
                var title = $scope.title;
                var description = $scope.tinymceModel;
                var itemsChosed = $scope.uploader.queue.length;

                if((title == null || title == "") && (description == null || description == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (description != null || description != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (description == null || description == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if(itemsChosed == 0) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Chose Portfolio Image!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {

                    var params = {
                        id: id,
                        title: title,
                        description: description
                    };

                    var token = userService.getCurrentToken();
                    HttpPostServices.postData(HttpPostServices.endpointEditPortfolio, params, token,
                        function(response) {

                            if(uploader.getNotUploadedItems().length != 0) {

                                var idPortfolio = $rootScope.editPortfolioId;

                                uploader.onBeforeUploadItem = function(item) {
                                    item.url = 'api/portfolio/images/add/' + idPortfolio;
                                };

                                $scope.isDisabled = true;
                                bsLoadingOverlayService.start();

                                var arr = uploader.getNotUploadedItems();
                                angular.forEach(arr, function (value, key) {
                                        uploader.uploadItem(value);
                                });

                                uploader.onCompleteItem = function (fileItem, response, status, headers) {
                                    //console.info('onCompleteItem', fileItem, response, status, headers);
                                    //console.log(response);
                                    //imageUrls.push();

                                };
                                uploader.onErrorItem = function (fileItem, response, status, headers) {
                                    //console.info('onErrorItem', fileItem, response, status, headers);
                                    bsLoadingOverlayService.stop();
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent("Can't upload! Too large picture size!")
                                            .position('bottom left')
                                            .hideDelay(1000)
                                    );
                                    $mdDialog.hide(answer);
                                };
                                uploader.onCompleteAll = function (fileItem, response, status, headers) {
                                    bsLoadingOverlayService.stop();

                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Successfully edited portfolio!')
                                            .position('bottom left')
                                            .hideDelay(1000)
                                    );

                                    $mdDialog.hide(answer);
                                };

                            } else {

                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Successfully edited blog!')
                                        .position('bottom left')
                                        .hideDelay(1000)
                                );

                                $mdDialog.hide(answer);
                            }
                        },
                        function(response) {
                            if(response.data.error == "token_not_provided") {
                                $mdDialog.hide('logout');
                                userService.logout();
                                $location.path('/admin');
                            }
                        }
                    );
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }

    };

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
        }
    };
}]);

weddingProjectControllers.controller('DeletePortfolioPopUpController', ['$scope', '$mdDialog', 'userService', 'HttpPostServices', '$rootScope', '$location', '$mdToast', function ($scope, $mdDialog, userService, HttpPostServices, $rootScope, $location, $mdToast) {
    $scope.answer = function(answer) {
        if(userService.checkIfLoggedIn() == true) {
            if(answer == 'yes') {
                id = $rootScope.deletePortfolioId;
                var token = userService.getCurrentToken();
                HttpPostServices.postData(HttpPostServices.endpointDeletePortfolio + id, id, token,
                    function(response) {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted portfolio!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        $mdDialog.hide(answer);
                    },
                    function(response) {
                        if(response.data.error == "token_not_provided") {
                            $mdDialog.hide('logout');
                            userService.logout();
                            $location.path('/admin');
                        }
                    }
                );
            } else if (answer == 'no') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };
}]);

weddingProjectControllers.controller('SliderAdminController', ['$scope', '$location', 'userService', 'FileUploader', 'HttpPostServices', 'userService', '$interval', '$mdToast', 'HttpGetServices', 'bsLoadingOverlayService', function ($scope, $location, userService, FileUploader, HttpPostServices, userService, $interval, $mdToast, HttpGetServices, bsLoadingOverlayService) {

    $scope.activated = true;

    $scope.determinateValue = 30;

    $interval(function() {

    $scope.determinateValue += 10;
    if ($scope.determinateValue > 100) {
      $scope.determinateValue = 10;
    }

    }, 100);

    $scope.removeFromDb = function(id) {
        if(userService.checkIfLoggedIn() == true) {
            var id = id;
            var token = userService.getCurrentToken();
            HttpPostServices.postData(HttpPostServices.endpointDeleteSliderImage + id, id, token,
                function(response) {
                },
                function(response) {
                    if(response.data.error == "token_not_provided") {
                        $mdDialog.hide('logout');
                        userService.logout();
                        $location.path('/admin');
                    }
                }
            );
        } else {
            userService.logout();
            $location.path('/admin');
        }
        
    }

    if(userService.checkIfLoggedIn() == true) {

        HttpGetServices.getListData(HttpGetServices.endpointSlider)
            .then(function(data) {
                for (var i = 0; i < data.length; i++) {
                    var image = new FileUploader.FileItem($scope.uploader, {
                        lastModifiedDate: data[i].updated_at,
                        size: 1e6,
                        type: data[i].image_url,
                        name: data[i].id,
                    });

                    image.progress = 1000;
                    image.isUploaded = true;
                    image.isSuccess = true;

                    $scope.uploader.queue.push(image);
                }
                $scope.activated = false;
        });


        var token = userService.getCurrentToken();

        $scope.uploader = new FileUploader( {
            url: 'api/upload/slider',
            queueLimit: 3,
            headers: {'Authorization': 'Bearer ' + token}
        });

        $scope.uploader.onWhenAddingFileFailed = function(item , filter, options) {
            // console.info('onWhenAddingFileFailed', item, filter, options);
        };
        $scope.uploader.onAfterAddingFile = function(fileItem) {
            // console.info('onAfterAddingFile', fileItem);
        };
        $scope.uploader.onAfterAddingAll = function(addedFileItems) {
            // console.info('onAfterAddingAll', addedFileItems);
        };
        $scope.uploader.onBeforeUploadItem = function(item) {
            // console.info('onBeforeUploadItem', item);

        };
        $scope.uploader.onProgressItem = function(fileItem, progress) {
            // console.info('onProgressItem', fileItem, progress);
        };
        $scope.uploader.onProgressAll = function(progress) {
            // console.info('onProgressAll', progress);
        };
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);
        };
        $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
            bsLoadingOverlayService.stop();
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Can't upload! Too large picture size!")
                    .position('bottom left')
                    .hideDelay(1000)
            );
        };
        $scope.uploader.onCancelItem = function(fileItem, response, status, headers) {
            // console.info('onCancelItem', fileItem, response, status, headers);
        };
        $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
            //console.log(response);
        };
        $scope.uploader.onCompleteAll = function() {
            bsLoadingOverlayService.stop();
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Successfully uploaded all images!')
                    .position('bottom left')
                    .hideDelay(1000)
            );

        };
        $scope.customUpload = function() {
            bsLoadingOverlayService.start();
            var arr = $scope.uploader.getNotUploadedItems();
            angular.forEach(arr, function(value, key) {
                $scope.uploader.uploadItem(value);
            });
        };

        $scope.currentNavItem = 'slider';
    } else {
        userService.logout();
        $location.path('/admin');
    }
    
}]);

weddingProjectControllers.controller('CalendarAdminController', ['$scope', '$location','$mdDialog', 'userService', '$rootScope', 'HttpGetServices', '$mdToast', function ($scope, $location,$mdDialog, userService, $rootScope, HttpGetServices, $mdToast) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.currentNavItem = 'calendar';

        //CONFIG CALENDAR
        $scope.events = [];

        $scope.eventSources = [$scope.events];
        //END - CONFIG CALENDAR
        HttpGetServices.getListData(HttpGetServices.endpointCalendarAll).then(function (response) {
            $scope.events.splice(0, $scope.events.length);
            $scope.bookedDates = response;
            angular.forEach($scope.bookedDates, function (value, key) {

                var booked =  '';
                if(value.booked == 0) {
                    booked = '#81a3af';
                } else {
                    booked = '#cf3333';
                }
                $scope.events.push({
                    id: value.id,
                    title: value.description,
                    start: new Date(value.start_date),
                    end: new Date(value.end_date),
                    className: 'calEvent',
                    color: booked,
                    allDay: true,
                    stick: true
                });
            });
        });
        $scope.eventClick = function(date, jsEvent, view, ev){

            if(userService.checkIfLoggedIn() == true) {

                $rootScope.bookedId = date.id;

                $mdDialog.show({
                    controller: 'DayChangesUpdatePopUpController',
                    templateUrl: 'partials/admin/popups/reserve-day-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                    .then(function(answer) {
                        if(answer == 'updated') {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Successfully edited event!')
                                    .position('bottom left')
                                    .hideDelay(1000)
                            );

                            HttpGetServices.getListData(HttpGetServices.endpointCalendarAll).then(function (response) {
                                $scope.events.splice(0, $scope.events.length);
                                $scope.bookedDates = response;
                                angular.forEach($scope.bookedDates, function (value, key) {

                                    var booked =  '';
                                    if(value.booked == 0) {
                                        booked = '#81a3af';
                                    } else {
                                        booked = '#cf3333';
                                    }
                                    $scope.events.push({
                                        id: value.id,
                                        title: value.description,
                                        start: new Date(value.start_date),
                                        end: new Date(value.end_date),
                                        className: 'calEvent',
                                        color: booked,
                                        allDay: true,
                                        stick: true
                                    });
                                });
                            });
                        } else if(answer == 'logout') {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('You have to login first!')
                                    .position('bottom left')
                                    .hideDelay(1000)
                            );
                        }
                        
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });

            } else {
                userService.logout();
                $location.path('/admin');
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('You have to login first!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            }
        };

        $scope.dayClick = function(date, ev) {

            if(userService.checkIfLoggedIn() == true) {

                $rootScope.firstDay = date._d;

                $mdDialog.show({
                    controller: 'DayChangesBlogPopUpController',
                    templateUrl: 'partials/admin/popups/reserve-day-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                    .then(function(answer) {
                        if(answer == 'reserved') {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Successfully booked event!')
                                    .position('bottom left')
                                    .hideDelay(1000)
                            );

                            HttpGetServices.getListData(HttpGetServices.endpointCalendarAll).then(function (response) {
                                $scope.events.splice(0, $scope.events.length);
                                $scope.bookedDates = response;
                                angular.forEach($scope.bookedDates, function (value, key) {

                                    var booked =  '';
                                    if(value.booked == 0) {
                                        booked = '#81a3af';
                                    } else {
                                        booked = '#cf3333';
                                    }
                                    $scope.events.push({
                                        id: value.id,
                                        title: value.description,
                                        start: new Date(value.start_date),
                                        end: new Date(value.end_date),
                                        className: 'calEvent',
                                        color: booked,
                                        allDay: true,
                                        stick: true
                                    });
                                });
                            });
                        } else if(answer == 'logout') {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('You have to login first!')
                                    .position('bottom left')
                                    .hideDelay(1000)
                            );
                        }
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });

            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('You have to login first!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
                userService.logout();
                $location.path('/admin');
            }
        };

        $scope.uiConfig = {
            calendar:{
                editable: false,
                header:{
                    left: '',
                    center: 'title',
                    right: 'today prev,next'
                },
                eventClick: $scope.eventClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                eventRender: $scope.eventRender,
                dayClick: $scope.dayClick
            }
        };


    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);
weddingProjectControllers.controller('DayChangesBlogPopUpController', ['$scope', '$mdDialog', '$rootScope', 'HttpPostServices', 'userService', '$mdToast', '$location', function ($scope, $mdDialog, $rootScope, HttpPostServices, userService, $mdToast, $location) {
    // $rootScope.bookedId

    $scope.checkboxModel = 0;
    $scope.tinymceModel = "";

    $scope.myDate = $rootScope.firstDay;
    $scope.addReserve = function () {
        if(userService.checkIfLoggedIn() == true) {

            function convert(str) {
                var date = new Date(str),
                    mnth = ("0" + (date.getMonth()+1)).slice(-2),
                    day  = ("0" + date.getDate()).slice(-2);
                return [ date.getFullYear(), mnth, day ].join("-");
            }
            var token = userService.getCurrentToken();
            var params = {
                start_date: convert($rootScope.firstDay),
                end_date: convert($scope.myDate),
                booked: $scope.checkboxModel,
                description: $scope.description
            };
            HttpPostServices.postData(HttpPostServices.endpointAddNewDate, params, token, function (response) {
                $mdDialog.hide('reserved');
                //Success
            }, function (response) {
                //error
                if(response.data.error == "token_not_provided") {
                    $mdDialog.hide('logout');
                    userService.logout();
                    $location.path('/admin');
                }
            });

        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
        
    };

    $scope.close = function() {
        $mdDialog.hide();
    }

}]);

weddingProjectControllers.controller('DayChangesUpdatePopUpController', ['$scope', '$mdDialog', '$rootScope', 'HttpPostServices', 'userService', 'HttpGetServices', '$mdToast', '$location', function ($scope, $mdDialog, $rootScope, HttpPostServices, userService, HttpGetServices, $mdToast, $location) {
    $scope.checkboxModel = 0;

    $scope.tinymceModel = "";
    HttpGetServices.getListData(HttpGetServices.endpointCalendarSingle + $rootScope.bookedId).then(function (response) {
        $scope.myDate = new Date(response.end_date);
        $scope.checkboxModel = response.booked;
        $scope.description = response.description;

        $scope.addReserve = function () {
            if(userService.checkIfLoggedIn() == true) {

                function convert(str) {
                    var date = new Date(str),
                        mnth = ("0" + (date.getMonth()+1)).slice(-2),
                        day  = ("0" + date.getDate()).slice(-2);
                    return [ date.getFullYear(), mnth, day ].join("-");
                }
                var token = userService.getCurrentToken();
                var params = {
                    id: $rootScope.bookedId,
                    start_date: convert(response.start_date),
                    end_date: convert($scope.myDate),
                    booked: $scope.checkboxModel,
                    description: $scope.description
                };
                HttpPostServices.postData(HttpPostServices.endpointCalendarUpdate, params, token, function (response) {
                    $mdDialog.hide('updated');
                    //Success
                }, function (response) {
                    //error
                    if(response.data.error == "token_not_provided") {
                        $mdDialog.hide('logout');
                        userService.logout();
                        $location.path('/admin');
                    }
                });

            } else {
                $mdDialog.hide('logout');
                userService.logout();
                $location.path('/admin');
            }
            
        };

        $scope.close = function() {
            $mdDialog.hide();
        }

    });



}]);
