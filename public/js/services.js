var weddingProjectServices = angular.module('weddingProjectServices', [
    'LocalStorageModule'
]);

weddingProjectServices.factory('userService', ['$http', 'localStorageService', function($http, localStorageService) {
    function checkIfLoggedIn() {

        if(localStorageService.get('token')) {
            return true;
        }
        else {
            return false;
        }

    }

    function login(email, password, onSuccess, onError) {

        var user = {
            email: email,
            password: password
        };

        $http.post('/login',user
        ).
        then(function(response) {
            console.log(response);
            localStorageService.set('token', response.data.token);
            onSuccess(response);

        }, function(response) {
            //console.log(response);
            onError(response);

        });

    }

    function logout() {

        localStorageService.remove('token');

    }

    function getCurrentToken() {
        return localStorageService.get('token');
    }


    return {
        checkIfLoggedIn: checkIfLoggedIn,
        login: login,
        logout: logout,
        getCurrentToken: getCurrentToken
    }

}]);

weddingProjectServices.service('HttpGetServices', ['$http', function($http) {


    function getListData(url) {

        return $http.get(url)
            .then(function(response) {
                return response.data;
        })
    }

    function getSingleData(url, id) {
        
        var urlSingle = url + id;
        
        return $http.get(urlSingle)
            .then(function(response) {
                return response.data;
        })
    }

    return {
        endpointBlogList: 'api/blogs',
        endpointPortfolioList: '/api/portfolios',
        endpointSingleBlog: '/api/blogs/',
        endpointSinglePortfolio: '/api/portfolios/',
        endpointSlider: '/api/slider/',
        endpointCalendarList: 'api/calendar',
        endpointCalendarSingle: 'api/calendar/',
        endpointCalendarAll: '/api/calendar/all',
        getListData: getListData,
        getSingleData: getSingleData
    }

}]);

weddingProjectServices.service('HttpPostServices', ['$http', function($http) {

    function postData(endpoint, params, token, onSuccess, onError) {
        
        $http.post(endpoint, params, 
            {
                headers: {'Authorization': 'Bearer ' + token}
            }
        ).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    return {
        endpointAddNewBlog: '/api/blogs/add',
        endpointAddNewPortfolio: '/api/portfolios/add',
        endpointEditBlog: '/api/blogs/update',
        endpointEditPortfolio:  '/api/portfolios/update',
        endpointDeleteBlog: 'api/blogs/delete/',
        endpointDeletePortfolio: 'api/portfolios/delete/',
        endpointAddNewDate: 'api/calendar/add',
        endpointDeletePortfolioImage: 'api/portfolio/image/delete/',
        endpointDeleteSliderImage: 'api/slider/delete/',
        endpointCalendarAdd: 'api/calendar/add',
        endpointCalendarDelete: 'api/calendar/delete/',
        endpointCalendarUpdate: 'api/calendar/update',
        endpointSendMail: 'api/mail',
        postData: postData
    }

}]);

weddingProjectServices.service('HideElementsService', ['$location', '$rootScope', function($location, $rootScope) {

    function hideElements() {
        // hide header on a specific route
        if($location.path() == '/admin' || $location.path() == '/admin/blog' || $location.path() == '/admin/portfolio' || $location.path() == '/admin/slider' || $location.path() == '/admin/calendar') {
              $rootScope.hideHeader = true;
        }
        else {
              $rootScope.hideHeader = false;
        }

        // hide footer on a specific route
        if($location.path() == '/admin' || $location.path() == '/admin/blog' || $location.path() == '/admin/portfolio' || $location.path() == '/admin/slider' || $location.path() == '/admin/calendar') {
              $rootScope.hideFooter = true;
        }
        else {
              $rootScope.hideFooter = false;
        }

        //hide Mainmenu in adminPanel
        if($location.path() == '/admin' || $location.path() == '/admin/blog' || $location.path() == '/admin/portfolio' || $location.path() == '/admin/slider' || $location.path() == '/admin/calendar') {
            $('.meniCheck').hide();

        }
        else {
            if($(window).width() < 767)
            $('.meniCheck').show();
        }

        //add active class in fixed menu
        if($location.path() == '/') {
              $('.home').addClass('active');
          } else {
              $('.home').removeClass('active');
          }

          if($location.path() == '/calendar') {
              $('.calendar').addClass('active');
          } else {
              $('.calendar').removeClass('active');
          }

          if($location.path() == '/portfolio') {
              $('.portfolio').addClass('active');
          } else {
              $('.portfolio').removeClass('active');
          }

          if($location.path() == '/blog') {
              $('.blog').addClass('active');
          } else {
              $('.blog').removeClass('active');
          }

          if($location.path() == '/contact') {
              $('.contact').addClass('active');
          } else {
              $('.contact').removeClass('active');
          }


        if($location.path() == '/') {
            $rootScope.headerSection = "header-abs";
        }

        //set header class based on current route
        var root = $location.path();
        switch(root) {
            case '/':
                $rootScope.headerSection = "header-abs";
                break;
            case '/portfolio':
                $rootScope.headerSection = "header-bg";
                break;
            case '/blog': 
                $rootScope.headerSection = "header-bg";
                break;
            case '/contact':
                $rootScope.headerSection = "header-bg";
                break;
            case '/privacy-policy':
                $rootScope.headerSection = "header-bg";
                break;
            default: $rootScope.headerSection = "header-bg";

        }
        
    }

    return {
        hideElements: hideElements
    }

}]);

weddingProjectServices.service('parseHtmlService', ['$sce', function($sce) {

    function parse(data) {
        return $sce.trustAsHtml(data);
    }

    return {
        parse: parse
    }

}]);